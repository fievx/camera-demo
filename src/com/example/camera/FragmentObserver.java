package com.example.camera;

public interface FragmentObserver {
	public void update (String param);
	public void startPreview(String path);
}
