package com.example.camera;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;

import com.commonsware.cwac.camera.PictureTransaction;
import com.commonsware.cwac.camera.SimpleCameraHost;

public class MyCameraHost extends SimpleCameraHost {
	private String photoName;
	private String extension = ".png";
	
	public MyCameraHost(Context _ctxt) {
		super(_ctxt);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected String getPhotoFilename() {
		// TODO Auto-generated method stub
		return this.photoName + this.extension;
	}

	public void setPhotoName (String name){
		this.photoName = name;
	}
	
	 
	 @Override
	 public boolean useSingleShotMode() {
	     return true;
	 }


	@Override
	protected File getPhotoPath() {
		// TODO Auto-generated method stub
		return super.getPhotoPath();
	}






	@Override
	public void saveImage(PictureTransaction xact, Bitmap bitmap) {
		// TODO Auto-generated method stub
		super.saveImage(xact, bitmap);
	}



}
