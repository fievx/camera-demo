package com.example.cameratest;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import com.example.camera.MyCameraActivity;
import com.squareup.picasso.Picasso;

public class MainActivity extends Activity{
	private Button cameraButton;
	private Context context;
	private ImageView photoView;
	public static final String TAG_CAMERA_FRAGMENT = "camera_fragment";
	public static final String TAG_PREVIEW_FRAGMENT = "preview_fragment";
	private static final int CODE_IMAGE_PATH = 1;
	public static final String TAG_IMAGE_PATH = "image_path";
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		setContentView(R.layout.activity_main);
		photoView = (ImageView) findViewById(R.id.saved_image_view);
		cameraButton = (Button) findViewById(R.id.retake_photo_button);
		cameraButton.setOnClickListener(new OnClickListener (){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent (context, MyCameraActivity.class);
				startActivityForResult(intent, CODE_IMAGE_PATH);
			}
			
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    // Handle the logic for the requestCode, resultCode and data returned...
		if (requestCode == CODE_IMAGE_PATH){
		switch (resultCode) {
		case Activity.RESULT_OK :
			String path = data.getStringExtra(TAG_IMAGE_PATH);
			Log.d("main Activity result", "result is OK and image is loading with path : " + path);
			File f = new File (path);
			Picasso.with(context).load(f).centerInside().fit().into(photoView);
			break;
		}
		
		}
	}
}
